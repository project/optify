<?php
/**
 * @file
 * Handles output of the Optify JavaScript tag.
 *
 * The Optify JavaScript tag is exposed to each page in the site unless
 * specific pages such as admin pages are set to not be tracked by Optify.
 */

/**
 * Implements hook_help().
 */
function optify_help($path, $arg) {
  switch ($path) {
    case 'admin/config/services/optify':
      return t("The Optify module will allow website managers (they do not need to have technical expertise) to quickly, easily, and seamlessly track traffic and leads to their website using the combination of the module and Optify.");
  }
}

/**
 * Implements hook_permissions().
 */
function optify_permission() {
  return array(
    'administer optify' => array(
      'title' => t('Administer Optify'),
      'description' => t('View Optify administration page.'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function optify_menu() {
  $items['admin/config/services/optify'] = array(
    'title' => 'Optify',
    'description' => 'Configure tracking Optify tracking code.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('optify_admin'),
    'access arguments' => array('administer optify'),
    'file' => 'optify.admin.inc',
  );
  return $items;
}

/**
 * Implements hook_page_alter().
 */
function optify_page_alter() {
  $token = variable_get('optify_embed_code');

  if (!_optify_no_display()) {
    drupal_add_js(array('optify' => array('token' => check_plain($token))), 'setting');
    drupal_add_js(drupal_get_path('module', 'optify') . '/optify.js');
  }
}

/**
 * Find out if the current page matches one of those we don't want to track with Optify.
 *
 * @return bool TRUE if Optify no display is set for this path
 */
function _optify_no_display() {
  $no_display = variable_get('optify_no_display', "admin\nadmin/*\nbatch\nnode/add*\nnode/*/*\nuser/*/*");

  // Convert path to lowercase. This allows comparison of the same path with
  // different case. Ex: /Page, /page, /PAGE.
  $no_display = drupal_strtolower($no_display);

  // Convert the Drupal path to lowercase.
  $path = drupal_strtolower(drupal_get_path_alias($_GET['q']));

  // Compare the lowercase internal and lowercase path alias (if any).
  return drupal_match_path($path, $no_display);
}
