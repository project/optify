The Optify module inserts a javascript tag for tracking user behavior in the
Optify digital marketing suite. Webmasters can configure Optify's tracking code
to not fire on specific Drupal pages, such as /admin.

Go to /admin/config/services/optify to configure.

About Optify:
Optify delivers a simple digital marketing software suite for agencies. Our 
complete cloud-based software empowers agency marketers to easily create and 
manage multiple lead generation programs, nurture prospects, prioritize the best
performing programs and streamline the reporting of client results - all from 
one login.

Optify includes integrated search engine optimization, email marketing, landing 
pages, lead intelligence and nurturing, social media marketing, contact 
management and a seamless integration with Drupal. In addition, large clients 
and advanced implementations may utilize the Optify API for delivering custom 
reporting and integrate with CRM systems including Salesforce.com.
